import { Component, OnDestroy } from '@angular/core';

import { Subject } from 'rxjs';

@Component({
  selector: 'app-base',
  template: '<div></div>'
})
export class BaseComponent implements OnDestroy {
  // destroy subject is handy for rxjs takeUntil
  destroy$: Subject<boolean> = new Subject<boolean>();

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
