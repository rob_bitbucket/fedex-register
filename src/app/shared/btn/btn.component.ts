import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-btn',
  templateUrl: './btn.component.html',
  styleUrls: ['./btn.component.scss']
})
export class BtnComponent {
  @ViewChild('button', { static: false }) button: ElementRef;
  @Input() btnType: string;
  @Input() btnClass: string;
  @Input() btnId: string;
  @Input() disabled: boolean;
  @Input() loading: boolean;
  @Input() iconName: string;
  @Output() readonly clicked: EventEmitter<any> = new EventEmitter();

  click(e: Event): void {
    if (!this.disabled) {
      this.clicked.emit(e);
    }
  }

  focus(): void {
    this.button.nativeElement.focus();
  }
}

// svg animation by Nikhil Krishnan
