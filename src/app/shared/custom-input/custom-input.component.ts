import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-custom-input',
  templateUrl: './custom-input.component.html',
  styleUrls: ['./custom-input.component.scss']
})
export class CustomInputComponent {
  @Output() readonly focusChange = new EventEmitter<any>();
  @Output() readonly blurChange = new EventEmitter<any>();
  @ViewChild('text', { static: false }) text;

  // generic
  @Input() control: any;
  @Input() customType: string;
  @Input() customClass: any;
  @Input() customPlaceholder: string;
  @Input() customId: string;

  // for checkbox / radio
  @Input() controlName: string;
  @Input() label: string;
  @Input() value: string;

  // used on filter list search
  setFocusOnText(): void {
    this.text.nativeElement.focus();
  }

  onFocus(e: any): void {
    this.focusChange.emit(e);
  }

  onBlur(e: any): void {
    this.blurChange.emit(e);
  }
}
