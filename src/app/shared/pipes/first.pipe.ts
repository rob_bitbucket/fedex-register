import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'first'
})
export class FirstPipe implements PipeTransform {
  // returns the first object of a collection
  transform(value: any, args?: any): any {
    if (value && value.length > 0) {
      return [value[0]];
    }
    return value;
  }
}
