import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'firstKey'
})
export class FirstKeyPipe implements PipeTransform {
  // returns the first key of a collection
  transform(value: any, args?: any): any {
    if (!value) {
      return;
    }

    const keys = Object.keys(value);
    if (keys && keys.length > 0) {
      return keys[0];
    }

    return null;
  }
}
