import { Component, Input, OnInit } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'app-show-error',
  templateUrl: './show-error.component.html',
  styleUrls: ['./show-error.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition('void => *', [
        style({ top: '10px', opacity: 0 }),
        animate('.25s ease-in-out', style({ top: '0px', opacity: 1 }))
      ]),
      transition('* => void', [
        animate('.25s ease-in-out'),
        style({ top: '-10px', opacity: 0 })
      ])
    ])
  ]
})
export class ShowErrorComponent implements OnInit {
  @Input() control: any;
  @Input() submitted: boolean;
  @Input() tooltip: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
