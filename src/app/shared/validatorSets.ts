//tslint:disable
import { Validators } from '@angular/forms';
import { CustomValidators } from 'src/app/shared/custom-validators';

export class ValidatorsSets {
  static passwordValidator = Validators.compose([
    Validators.required,
    Validators.minLength(8),
    CustomValidators.capitalLetter(),
    CustomValidators.lowerLetter()
  ]);
}
