import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomInputComponent } from 'src/app/shared/custom-input/custom-input.component';
import { FirstPipe } from 'src/app/shared/pipes/first.pipe';
import { ShowErrorComponent } from 'src/app/shared/show-error/show-error.component';
import { BtnComponent } from 'src/app/shared/btn/btn.component';
import { SvgLoadingDotsComponent } from 'src/app/shared/svg-loading-dots/svg-loading-dots.component';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PasswordStrengthComponent } from 'src/app/shared/password-strength/password-strength.component';
import { FirstKeyPipe } from 'src/app/shared/pipes/first-key.pipe';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  declarations: [
    BaseComponent,
    BtnComponent,
    CustomInputComponent,
    FirstPipe,
    FirstKeyPipe,
    PasswordStrengthComponent,
    SvgLoadingDotsComponent,
    ShowErrorComponent
  ],
  exports: [
    BaseComponent,
    BtnComponent,
    CustomInputComponent,
    FirstPipe,
    FirstKeyPipe,
    PasswordStrengthComponent,
    SvgLoadingDotsComponent,
    ShowErrorComponent
  ]
})
export class SharedModule { }
