import { FormControl } from '@angular/forms';

export class CustomValidators {

  // use positive look ahead to see if at least one lower case letter exists
  static capitalLetter(): any {
    return (control: FormControl) => {
      if (control && /(?=.*[A-Z])/.test(control.value) !== true) {
        return { capitalLetter: true };
      }
      return null;
    };
  }

  // use positive look ahead to see if at least one upper case letter exists
  static lowerLetter(): any {
    return (control: FormControl) => {
      if (control && /(?=.*[a-z])/.test(control.value) !== true) {
        return { lowerLetter: true };
      }
      return null;
    };
  }


  // custom email validator, name@provider.com is valid, not name@provider
  static email(): any {
    return (control: FormControl) => {
      if (control && /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,10}$/.test(control.value) !== true) {
        return { email: true };
      }
      return null;
    };
  }
}
