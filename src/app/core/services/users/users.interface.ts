import { Observable } from 'rxjs';
import { PostUser } from 'src/app/core/services/users/_types/postUser';

export interface UsersServiceInterface {
  apiUrl: string;
  postRegister(model: PostUser): Observable<any>;
}
