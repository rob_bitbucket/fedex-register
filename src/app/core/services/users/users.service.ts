import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { UsersServiceInterface } from 'src/app/core/services/users/users.interface';
import { PostUser } from 'src/app/core/services/users/_types/postUser';

@Injectable({
  providedIn: 'root'
})
export class UsersService implements UsersServiceInterface {
  // in real situation this endpoint would come from a config.json
  // loaded by app initializer
  apiUrl = 'https://demo-api.now.sh/users';

  constructor(
    private httpClient: HttpClient
  ) { }

  postRegister(model: PostUser): Observable<any> {
    const url = `${this.apiUrl}/`;
    return this.httpClient.post<any>(url, model);
  }
}
