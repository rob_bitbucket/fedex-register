export interface PostUser {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}
