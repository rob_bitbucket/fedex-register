import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { UsersServiceInterface } from 'src/app/core/services/users/users.interface';
import { PostUser } from 'src/app/core/services/users/_types/postUser';

@Injectable({
  providedIn: 'root'
})
export class UsersServiceMock implements UsersServiceInterface {
  apiUrl;
  postRegister = (model: PostUser): Observable<any> => new Observable();
}
