import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
  AUTH_ROUTE,
} from 'src/app/auth/auth.routes';

const routes: Routes = [
  {
    path: '',
    redirectTo: AUTH_ROUTE,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  providers: [],
  exports: [RouterModule]
})
export class AppRoutingModule { }
