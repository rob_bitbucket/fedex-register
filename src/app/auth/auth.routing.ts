import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthRegisterComponent } from 'src/app/auth/auth-register/auth-register.component';
import {
  AUTH_REGISTER_ROUTE,
  AUTH_ROUTE,
} from 'src/app/auth/auth.routes';
import { AuthComponent } from 'src/app/auth/auth/auth.component';

const routes: Routes = [
  {
    path: AUTH_ROUTE,
    component: AuthComponent,
    children: [
      {
        path: '',
        redirectTo: AUTH_REGISTER_ROUTE,
        pathMatch: 'full'
      },
      {
        path: AUTH_REGISTER_ROUTE,
        component: AuthRegisterComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  providers: [],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
