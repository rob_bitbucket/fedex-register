import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthComponent } from 'src/app/auth/auth/auth.component';
import { AuthRegisterComponent } from 'src/app/auth/auth-register/auth-register.component';
import { RouterModule } from '@angular/router';
import { AuthRoutingModule } from 'src/app/auth/auth.routing';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    ReactiveFormsModule,
    AuthRoutingModule
  ],
  declarations: [
    AuthComponent,
    AuthRegisterComponent
  ]
})
export class AuthModule { }
