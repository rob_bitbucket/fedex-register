import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { UsersService } from 'src/app/core/services/users/users.service';
import { PostUser } from 'src/app/core/services/users/_types/postUser';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { CustomValidators } from 'src/app/shared/custom-validators';
import { ValidatorsSets } from 'src/app/shared/validatorSets';
import { catchError, takeUntil } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-auth-register',
  templateUrl: './auth-register.component.html',
  styleUrls: ['./auth-register.component.scss']
})
export class AuthRegisterComponent extends BaseComponent {
  form: FormGroup;
  error: string;

  constructor(
    private fb: FormBuilder,
    private usersService: UsersService
  ) {
    super();

    this.form = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, CustomValidators.email()])],
      password: ['', ValidatorsSets.passwordValidator]
    });
  }

  submit(form: FormGroup): void {
    form.markAllAsTouched();
    this.error = null;

    if (!form.valid) {
      return;
    }

    if (
      this.password.value.toLocaleLowerCase().indexOf(this.firstName.value.trim().replace(/ /g, '').toLocaleLowerCase()) > -1 ||
      this.password.value.toLocaleLowerCase().indexOf(this.lastName.value.trim().replace(/ /g, '').toLocaleLowerCase()) > -1) {
      this.error = 'Password cannot contain first or lastname';
      return;
    }

    const model: PostUser = this.form.value;

    form.disable();

    this.usersService.postRegister(model)
      .pipe(
        catchError(err => {
          this.form.enable();
          return throwError(err);
        }),
        takeUntil(this.destroy$)
      )
      .subscribe(res => {
        // do some logic
      });
  }

  get firstName(): FormControl {
    return this.form.get('firstName') as FormControl;
  }

  get lastName(): FormControl {
    return this.form.get('lastName') as FormControl;
  }

  get email(): FormControl {
    return this.form.get('email') as FormControl;
  }

  get password(): FormControl {
    return this.form.get('password') as FormControl;
  }
}
