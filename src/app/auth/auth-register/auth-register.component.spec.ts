/* tslint:disable:no-unused-variable */
import { ComponentFixture, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AuthRegisterComponent } from './auth-register.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UsersService } from 'src/app/core/services/users/users.service';
import { UsersServiceMock } from 'src/app/core/services/users/users.service.mock';
import { CustomInputComponent } from 'src/app/shared/custom-input/custom-input.component';
import { of } from 'rxjs';
import { BtnComponent } from 'src/app/shared/btn/btn.component';

describe('AuthRegisterComponent', () => {
  let component: AuthRegisterComponent;
  let fixture: ComponentFixture<AuthRegisterComponent>;
  let service;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ],
      declarations: [
        CustomInputComponent,
        BtnComponent,
        AuthRegisterComponent
      ],
      providers: [
        {
          provide: UsersService,
          useClass: UsersServiceMock
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthRegisterComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(UsersService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return an invalid form when entering nothing', () => {
    expect(component.form.valid).toBeFalsy();
  });

  it('should return an invalid form when entering a invalid email', () => {
    const email = component.form.get('email');
    email.setValue('wrongemail');
    expect(component.form.valid).toBeFalsy();
  });

  it('should return a valid form control when entering a valid email', () => {
    const email = component.form.get('email');
    email.setValue('test@test.com');
    expect(email.valid).toBeTruthy();
  });

  it('should return an invalid password form control when entering only lower letters', () => {
    const password = component.form.get('password');
    password.setValue('abcdefqh');
    expect(password.valid).toBeFalsy();
  });

  it('should return an invalid password form control when entering only capital letters', () => {
    const password = component.form.get('password');
    password.setValue('ABCDEFGH');
    expect(password.valid).toBeFalsy();
  });

  it('should return an invalid password form control when entering only digits', () => {
    const password = component.form.get('password');
    password.setValue('12345678');
    expect(password.valid).toBeFalsy();
  });

  it('should return a valid password form control when entering at least 8 characters, 1 Capital, 1 Lower', () => {
    const password = component.form.get('password');
    password.setValue('Ll123456');
    expect(password.valid).toBeTruthy();
  });

  it('should throw error when firstName is used inside password', () => {
    component.form.get('firstName').setValue('Rob');
    component.form.get('lastName').setValue('van Eck');
    component.form.get('email').setValue('test@test.com');
    component.form.get('password').setValue('Test1234Rob');
    component.submit(component.form);
    expect(component.error).toBe('Password cannot contain first or lastname');
  });

  it('should throw error when lastName is used inside password', () => {
    component.form.get('firstName').setValue('Rob');
    component.form.get('lastName').setValue('van Eck');
    component.form.get('email').setValue('test@test.com');
    component.form.get('password').setValue('Test1234vanEck');
    component.submit(component.form);
    expect(component.error).toBe('Password cannot contain first or lastname');
  });

  it('should have form binding with firstName', () => {
    const el = fixture.debugElement.nativeElement.querySelector('#firstName');
    const control = component.form.get('firstName');
    const dummyValue = 'Rob';
    control.setValue(dummyValue);
    expect(el.value).toEqual(dummyValue);
  });

  it('should have form binding with lastName', () => {
    const el = fixture.debugElement.nativeElement.querySelector('#lastName');
    const control = component.form.get('lastName');
    const dummyValue = 'van Eck';
    control.setValue(dummyValue);
    expect(el.value).toEqual(dummyValue);
  });

  it('should have form binding with email', () => {
    const el = fixture.debugElement.nativeElement.querySelector('#email');
    const control = component.form.get('email');
    const dummyValue = 'email@test.nl';
    control.setValue(dummyValue);
    expect(el.value).toEqual(dummyValue);
  });

  it('should have form binding with password', () => {
    const el = fixture.debugElement.nativeElement.querySelector('#password');
    const control = component.form.get('password');
    const dummyValue = 'MijnWachtwoord';
    control.setValue(dummyValue);
    expect(el.value).toEqual(dummyValue);
  });

  it('should submit form, call userService.postRegister()', () => {
    expect(component.form.valid).toBeFalsy();
    component.form.setValue({
      firstName: 'Rob',
      lastName: 'van Eck',
      email: 'rob@robvaneckdesign.nl',
      password: 'Test1234'
    });

    expect(component.form.valid).toBeTruthy();
    const spy = spyOn(service, 'postRegister').and.returnValue(of({}));
    const elBtn = fixture.debugElement.nativeElement.querySelector('#submitBtn');
    elBtn.click();
    expect(service.postRegister).toHaveBeenCalled();
  });
});
